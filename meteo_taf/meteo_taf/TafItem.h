#pragma once
#include "taf-data.pb.h"
#include <qvariant.h>
#include <qlist.h>

class TafItem
{
private:
	TafData m_data;
	QList<TafItem*> m_childItems;
	TafItem *m_parentItem;
	_int32 probability;
	_int32 keyword;
public:
	TafItem(const TafData &data, TafItem *parentItem = 0);
	~TafItem();
	void appendChild(TafItem *child);
	TafItem *child(int row);
	int childCount() const;
	QVariant data(int column) const;
	int row() const;
	TafItem *parentItem();
};

