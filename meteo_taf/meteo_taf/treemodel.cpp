﻿#include "treemodel.hpp"
#include "HierarchicalHeaderView.h"

void TreeModel::fillHeaderModel(QStandardItemModel& headerModel)
{
	QStandardItem *Header1 = new QStandardItem(QObject::tr("Аэропорт"));
	QStandardItem *Header2 = new QStandardItem(QObject::tr("День\nсоставления\nпрогноза"));
	QStandardItem *Header3 = new QStandardItem(QObject::tr("Время\nсоставления\nпрогноза\nUTC"));
	QStandardItem *Header4 = new QStandardItem(QObject::tr("Прогноз действителен"));
	QStandardItem *Header5 = new QStandardItem(QObject::tr("Вероятность"));
	QStandardItem *Header6 = new QStandardItem(QObject::tr("Ветер"));
	QStandardItem *Header7 = new QStandardItem(QObject::tr("Видимость"));
	QStandardItem *Header8 = new QStandardItem(QObject::tr("Явления\nпогоды"));
	QStandardItem *Header9 = new QStandardItem(QObject::tr("Облачность"));
	QStandardItem *Header10 = new QStandardItem(QObject::tr("Температура"));

	QList<QStandardItem *> l;

	{
		l.push_back(new QStandardItem(tr("Дата\nначала\nпрогноза")));
		Header4->appendColumn(l);
		l.clear();
		l.push_back(new QStandardItem(tr("Время\nначала\nпрогноза\nUTC")));
		Header4->appendColumn(l);
		l.clear();
		l.push_back(new QStandardItem(tr("Дата\nконца\nпрогноза")));
		Header4->appendColumn(l);
		l.clear();
		l.push_back(new QStandardItem(tr("Время\nконца\nпрогноза\nUTC")));
		Header4->appendColumn(l);
		l.clear();
	}

	{
		l.push_back(new QStandardItem(tr("Скорость\nветра")));
		Header6->appendColumn(l);
		l.clear();
		l.push_back(new QStandardItem(tr("Направление\nветра")));
		Header6->appendColumn(l);
		l.clear();
		l.push_back(new QStandardItem(tr("Порыв\nветра")));
		Header6->appendColumn(l);
		l.clear();
	}

	{
		l.push_back(new QStandardItem(tr("Количество\nоблаков")));
		Header9->appendColumn(l);
		l.clear();
		l.push_back(new QStandardItem(tr("Вертикальная\nвидимость")));
		Header9->appendColumn(l);
		l.clear();
	}

	{
		l.push_back(new QStandardItem(tr("Минимальная\nтемпертаура")));
		Header10->appendColumn(l);
		l.clear();
		l.push_back(new QStandardItem(tr("Срок")));
		Header10->appendColumn(l);
		l.clear();
		l.push_back(new QStandardItem(tr("Минимальная\nтемпертаура")));
		Header10->appendColumn(l);
		l.clear();
		l.push_back(new QStandardItem(tr("Срок")));
		Header10->appendColumn(l);
		l.clear();
	}

	//Add all the table header items to to table header model.
	headerModel.setItem(0, 0, Header1);
	headerModel.setItem(0, 1, Header2);
	headerModel.setItem(0, 2, Header3);
	headerModel.setItem(0, 3, Header4);
	headerModel.setItem(0, 4, Header5);
	headerModel.setItem(0, 5, Header6);
	headerModel.setItem(0, 6, Header7);
	headerModel.setItem(0, 7, Header8);
	headerModel.setItem(0, 8, Header9);
	headerModel.setItem(0, 9, Header10);
	//END ADDING HEADERS
}
Qt::ItemFlags TreeModel::flags(const QModelIndex & index) const
{
	if (!index.isValid())
		return 0;
	return QAbstractItemModel::flags(index);
}
TreeModel::TreeModel(QObject * parent) : QAbstractItemModel(parent) {
	fillHeaderModel(_horizontalHeaderModel);
}

TreeModel::~TreeModel() {
	
}

int TreeModel::columnCount(const QModelIndex &parent) const {
	return 19;
}

int TreeModel::rowCount(const QModelIndex & parent) const
{
	return 4;
}

QVariant TreeModel::data(const QModelIndex & index, int role) const
{
	if (!index.isValid())
		return QVariant();
	if (role != Qt::DisplayRole)
		return QVariant();
	TafItem *item = static_cast<TafItem*>(index.internalPointer());
	return item->data(index.column());
}

QModelIndex TreeModel::index(int row, int column, const QModelIndex & parent) const
{
	TafItem *parentItem = m_root;
	if (parent.isValid())
	{
		parentItem = static_cast<TafItem *>(parent.internalPointer());
	}
	if (parentItem->childCount() > row)
		if (parentItem->child(row))
		{
			return createIndex(row, column, parentItem->child(row));
		}
	return QModelIndex();

}

QModelIndex TreeModel::parent(const QModelIndex &index) const {
	if (!index.isValid())
		return QModelIndex();

	TafItem *childItem = static_cast<TafItem*>(index.internalPointer());
	TafItem *parentItem = childItem->parentItem();

	if (parentItem == m_root)
		return QModelIndex();

	return createIndex(parentItem->row(), 0, parentItem);
}
