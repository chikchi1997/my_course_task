#include "mainwindow.h"
#include "treemodel.hpp"

MainWindow::MainWindow(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	this->setWindowTitle(tr("Табличное представление сводок METAR"));
	model = new TreeModel();
	setCentralWidget(ui.treeView);
	ui.treeView->setModel(model);
}

MainWindow::~MainWindow()
{

}
