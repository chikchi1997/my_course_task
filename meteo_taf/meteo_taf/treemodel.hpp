﻿#pragma once
#include <QAbstractItemModel>
#include <qstandarditemmodel.h>
#include "TafItem.h"
class TreeModel : public QAbstractItemModel {
	Q_OBJECT

public:
	QStandardItemModel _horizontalHeaderModel;
	void fillHeaderModel(QStandardItemModel &headerModel);
	int columnCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
	int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
	QVariant data(const QModelIndex & index, int role = Qt::DisplayRole)  const Q_DECL_OVERRIDE;
	QModelIndex index(int row, int column, const QModelIndex & parent = QModelIndex())  const Q_DECL_OVERRIDE;
	Qt::ItemFlags flags(const QModelIndex & index)  const Q_DECL_OVERRIDE;
	QModelIndex parent(const QModelIndex &index) const Q_DECL_OVERRIDE;
	TreeModel(QObject * parent = Q_NULLPTR);
	~TreeModel();

private:
	TafItem *m_root;
	
};
