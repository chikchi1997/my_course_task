#include "TafItem.h"
#include <qcoreapplication.h>
TafItem::TafItem(const TafData & data, TafItem * parentItem)
{
	m_parentItem = parentItem;
	m_data = data;

}

TafItem::~TafItem()
{
	qDeleteAll(m_childItems);
}

void TafItem::appendChild(TafItem * child)
{
	m_childItems.append(child);
}

TafItem * TafItem::child(int row)
{
	return m_childItems.value(row);
}

int TafItem::childCount() const
{
	return m_childItems.count();
}

QVariant TafItem::data(int column) const
{
	switch (column)
	{
	case 0:
		return QString::fromStdString(m_data.airfield_id());
	case 1:
		return QString::fromStdString(m_data.day());
	case 2:
		return QString(QString::number(m_data.hour()) + ':' + QString::number(m_data.minute())); 
	case 3:
		return QString::fromStdString(m_data.forecast_day_start());
	case 4:
		return m_data.forecast_hour_end();
	case 5:
		return QString::fromStdString(m_data.forecast_day_end());
	case 6:
		return m_data.forecast_hour_end();
	case 7:
		return QVariant();
	case 8:
		return m_data.wind_speed();
	case 9:
		if (m_data.is_variable() == false) {
			return m_data.wind_direction();
		}
		else
		{
			return QString("����������");
		}
	case 10:
		return m_data.max_wind_speed();
	case 11:
		if (!m_data.cavok())
		{
			return m_data.prevailing_visibility();
		}
		else
		{
			return QString(">10000");
		}
	case 12:
		if (!m_data.cavok())
		{
			return QString::fromStdString(m_data.present_weather());
		}
		else
		{
			return QVariant();
		}
	case 13:
		if (m_data.cavok()) {
			return QVariant();
		}
		else
		{
			return QString::fromStdString(m_data.cloud_amount());
		}
	case 14:
		if (m_data.cavok()) {
			return QVariant();
		}
		else
		{
			return QString::fromStdString(m_data.vertical_visibility());
		}
	case 15:
		return QVariant();
	case 16:
		return QVariant();
	case 17:
		return QVariant();
	case 18:
		return QVariant();
	default:
		break;
	}
	return QVariant();
}

int TafItem::row() const
{
	if (m_parentItem)
		return m_parentItem->m_childItems.indexOf(const_cast<TafItem*>(this));
	return 0;
}

TafItem * TafItem::parentItem()
{
	return m_parentItem;
}
