#ifndef TABLEMODEL_H
#define TABLEMODEL_H
#include <QAbstractItemModel>
#include "HierarchicalHeaderView.h"
#include <QStandardItemModel>
class TableModel : public QAbstractTableModel
{
public:
    QStandardItemModel _horizontalHeaderModel;
    QStandardItemModel _verticalHeaderModel;
    void fillHeaderModel(QStandardItemModel &headerModel);
    TableModel(QObject *parent = 0);
    int rowCount(const QModelIndex &) const;
    int columnCount(const QModelIndex &) const;
    QVariant data(const QModelIndex &index, int role) const;
};

#endif // TABLEMODEL_H
