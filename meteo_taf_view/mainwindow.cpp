#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QHeaderView>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    table = new TableModel();
    HierarchicalHeaderView* hv=new HierarchicalHeaderView(Qt::Horizontal, ui->tableView);
    hv->setHighlightSections(true);
    hv->setSectionsClickable(true);
    ui->tableView->setHorizontalHeader(hv);
    ui->tableView->setModel(table);
    ui->tableView->setWordWrap(true);
    ui->tableView->resizeColumnsToContents();
    ui->tableView->resizeRowsToContents();
    setCentralWidget(ui->tableView);
}

MainWindow::~MainWindow()
{
    delete ui;
}
